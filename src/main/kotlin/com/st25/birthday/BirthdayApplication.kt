package com.st25.birthday

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BirthdayApplication

fun main(args: Array<String>) {
	runApplication<BirthdayApplication>(*args)
}
